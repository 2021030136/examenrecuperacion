/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JDialog.java to edit this template
 */
package vista;

/**
 *
 * @author bmfil
 */
public class JdlRecibo extends javax.swing.JDialog {

    /**
     * Creates new form JdlRecibo
     */
    public JdlRecibo(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        txtkiloConsu = new javax.swing.JTextField();
        txtnumrecibo = new javax.swing.JTextField();
        txtfecha = new javax.swing.JTextField();
        txtnombre = new javax.swing.JTextField();
        txtcostoKilo = new javax.swing.JTextField();
        txtdomicilio = new javax.swing.JTextField();
        cbxtipoSer = new javax.swing.JComboBox<>();
        btncerrar = new javax.swing.JButton();
        btnnuevo = new javax.swing.JButton();
        btnguardar = new javax.swing.JButton();
        btnmostrar = new javax.swing.JButton();
        btnlimpiar = new javax.swing.JButton();
        btncancelar = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jlsubTotal = new javax.swing.JLabel();
        jlImpuesto = new javax.swing.JLabel();
        jlTotalPagar = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(null);

        jLabel2.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel2.setText("Num.Recibo");
        getContentPane().add(jLabel2);
        jLabel2.setBounds(30, 40, 130, 20);

        jLabel3.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel3.setText("Fecha");
        getContentPane().add(jLabel3);
        jLabel3.setBounds(240, 40, 70, 20);

        jLabel4.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel4.setText("Nombre");
        getContentPane().add(jLabel4);
        jLabel4.setBounds(30, 90, 110, 20);

        jLabel5.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel5.setText("Domicilio");
        getContentPane().add(jLabel5);
        jLabel5.setBounds(30, 130, 110, 20);

        jLabel6.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel6.setText("Tipo de servicio");
        getContentPane().add(jLabel6);
        jLabel6.setBounds(30, 200, 150, 20);

        jLabel7.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel7.setText("Costo Kilo whatss");
        getContentPane().add(jLabel7);
        jLabel7.setBounds(310, 200, 140, 20);

        jLabel8.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel8.setText("Kilowhatss consumido");
        getContentPane().add(jLabel8);
        jLabel8.setBounds(40, 250, 180, 20);

        txtkiloConsu.setEnabled(false);
        getContentPane().add(txtkiloConsu);
        txtkiloConsu.setBounds(230, 250, 240, 22);

        txtnumrecibo.setEnabled(false);
        txtnumrecibo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtnumreciboActionPerformed(evt);
            }
        });
        getContentPane().add(txtnumrecibo);
        txtnumrecibo.setBounds(150, 40, 63, 22);

        txtfecha.setEnabled(false);
        txtfecha.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtfechaActionPerformed(evt);
            }
        });
        getContentPane().add(txtfecha);
        txtfecha.setBounds(310, 40, 130, 22);

        txtnombre.setEnabled(false);
        getContentPane().add(txtnombre);
        txtnombre.setBounds(130, 90, 310, 22);

        txtcostoKilo.setEnabled(false);
        getContentPane().add(txtcostoKilo);
        txtcostoKilo.setBounds(450, 200, 100, 22);

        txtdomicilio.setEnabled(false);
        getContentPane().add(txtdomicilio);
        txtdomicilio.setBounds(130, 130, 310, 22);

        cbxtipoSer.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1 Residencial", "2 Comercial", "3 Industrial", " " }));
        cbxtipoSer.setEnabled(false);
        getContentPane().add(cbxtipoSer);
        cbxtipoSer.setBounds(170, 210, 120, 22);

        btncerrar.setText("cerrar");
        getContentPane().add(btncerrar);
        btncerrar.setBounds(290, 510, 110, 40);

        btnnuevo.setText("Nuevo");
        btnnuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnnuevoActionPerformed(evt);
            }
        });
        getContentPane().add(btnnuevo);
        btnnuevo.setBounds(470, 30, 100, 40);

        btnguardar.setText("guardar");
        btnguardar.setEnabled(false);
        getContentPane().add(btnguardar);
        btnguardar.setBounds(470, 80, 100, 40);

        btnmostrar.setText("mostrar");
        btnmostrar.setEnabled(false);
        getContentPane().add(btnmostrar);
        btnmostrar.setBounds(470, 140, 100, 40);

        btnlimpiar.setText("limpiar");
        btnlimpiar.setEnabled(false);
        getContentPane().add(btnlimpiar);
        btnlimpiar.setBounds(30, 510, 100, 40);

        btncancelar.setText("cancelar");
        btncancelar.setEnabled(false);
        getContentPane().add(btncancelar);
        btncancelar.setBounds(170, 510, 100, 40);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Calculos de luz", javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Segoe UI", 0, 14))); // NOI18N
        jPanel1.setLayout(null);

        jLabel9.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel9.setText("Subtotal");
        jPanel1.add(jLabel9);
        jLabel9.setBounds(10, 30, 120, 20);

        jLabel10.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel10.setText("Impuesto");
        jPanel1.add(jLabel10);
        jLabel10.setBounds(10, 60, 90, 20);

        jLabel11.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel11.setText("Total a pagar");
        jPanel1.add(jLabel11);
        jLabel11.setBounds(10, 90, 130, 20);

        jlsubTotal.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jPanel1.add(jlsubTotal);
        jlsubTotal.setBounds(140, 30, 110, 20);

        jlImpuesto.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jPanel1.add(jlImpuesto);
        jlImpuesto.setBounds(120, 60, 110, 20);

        jlTotalPagar.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jPanel1.add(jlTotalPagar);
        jlTotalPagar.setBounds(130, 90, 110, 30);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(30, 310, 510, 140);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtnumreciboActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtnumreciboActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtnumreciboActionPerformed

    private void txtfechaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtfechaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtfechaActionPerformed

    private void btnnuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnnuevoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnnuevoActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(JdlRecibo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(JdlRecibo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(JdlRecibo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(JdlRecibo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                JdlRecibo dialog = new JdlRecibo(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton btncancelar;
    public javax.swing.JButton btncerrar;
    public javax.swing.JButton btnguardar;
    public javax.swing.JButton btnlimpiar;
    public javax.swing.JButton btnmostrar;
    public javax.swing.JButton btnnuevo;
    public javax.swing.JComboBox<String> cbxtipoSer;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    public javax.swing.JLabel jlImpuesto;
    public javax.swing.JLabel jlTotalPagar;
    public javax.swing.JLabel jlsubTotal;
    public javax.swing.JTextField txtcostoKilo;
    public javax.swing.JTextField txtdomicilio;
    public javax.swing.JTextField txtfecha;
    public javax.swing.JTextField txtkiloConsu;
    public javax.swing.JTextField txtnombre;
    public javax.swing.JTextField txtnumrecibo;
    // End of variables declaration//GEN-END:variables
}
