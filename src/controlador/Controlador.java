/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package controlador;

import modelo.Recibo;
import vista.JdlRecibo;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author bmfil
 */
public class Controlador implements ActionListener {

    private Recibo rec;
    private JdlRecibo vista;

    public Controlador(Recibo rec, JdlRecibo vista) {
        this.vista = vista;
        this.rec = rec;
        vista.btncancelar.addActionListener(this);
        vista.btncerrar.addActionListener(this);
        vista.btnguardar.addActionListener(this);
        vista.btnlimpiar.addActionListener(this);
        vista.btnmostrar.addActionListener(this);
        vista.btnnuevo.addActionListener(this);
        vista.cbxtipoSer.addActionListener(this);

    }
    //inicializamos la vista

    private void iniciarVista() {
        vista.setSize(700, 700);
        vista.setVisible(true);
    }

    //btn nuevo
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == vista.btnnuevo) {
            vista.btncancelar.setEnabled(true);
            vista.btnguardar.setEnabled(true);
            vista.btnlimpiar.setEnabled(true);
            vista.btnmostrar.setEnabled(true);
            vista.txtnombre.setEnabled(true);
            vista.txtkiloConsu.setEnabled(true);
            vista.txtnumrecibo.setEnabled(true);
            vista.txtfecha.setEnabled(true);
            vista.cbxtipoSer.setEnabled(true);
            vista.txtdomicilio.setEnabled(true);
        }
        //btn cerrar
        if (e.getSource() == vista.btncerrar) {
            int respuesta = JOptionPane.showConfirmDialog(vista, "¿Quieres salir",
                    "Salir", JOptionPane.YES_NO_OPTION);
            if (respuesta == JOptionPane.YES_NO_OPTION) {
                vista.dispose();
                System.exit(0);
            }
        }
            //btn limpiar
            if (e.getSource() == vista.btnlimpiar) {
                vista.txtnombre.setText("");
                vista.txtkiloConsu.setText("");
                vista.txtnumrecibo.setText("");
                vista.txtfecha.setText("");
                vista.txtdomicilio.setText("");
                vista.txtcostoKilo.setText("");
            }
            //btn cancelar 
            if (e.getSource() == vista.btncancelar) {
                vista.txtnombre.setText("");
                vista.txtkiloConsu.setText("");
                vista.txtnumrecibo.setText("");
                vista.txtfecha.setText("");
                vista.txtdomicilio.setText("");
                vista.txtcostoKilo.setText("");
                

                vista.btncancelar.setEnabled(false);
                vista.btnguardar.setEnabled(false);
                vista.btnlimpiar.setEnabled(false);
                vista.btnmostrar.setEnabled(false);
                vista.txtnombre.setEnabled(false);
                vista.txtkiloConsu.setEnabled(false);
                vista.txtnumrecibo.setEnabled(false);
                vista.txtfecha.setEnabled(false);
                vista.cbxtipoSer.setEnabled(false);
                vista.txtdomicilio.setEnabled(false);
            }
            //btn guardar
            if(e.getSource()== vista.btnguardar){
                try{
        rec.setDomicilio(vista.txtdomicilio.getText());
            rec.setNombre(vista.txtnombre.getText());
            rec.setTipoServicio(vista.cbxtipoSer.getSelectedIndex());
            rec.setKiloConsu(Integer.parseInt(vista.txtkiloConsu.getText()));
            rec.setNumRecibo(Integer.parseInt(vista.txtnumrecibo.getText()));
            rec.setCostoKilo(Integer.parseInt(vista.txtcostoKilo.getText()));
            rec.setFecha(vista.txtfecha.getText());
            JOptionPane.showMessageDialog(vista, "Guardado con exito");
                }catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(vista, "Surgio el siguiente error" + ex.getMessage());
            } catch (Exception ex2) {
                JOptionPane.showMessageDialog(vista, "Surgio el siguiente error" + ex2.getMessage());
            }
    }
            //btn mostrar 
            if (e.getSource()== vista.btnmostrar){
                vista.txtdomicilio.setText(String.valueOf(rec.getDomicilio()));
            vista.txtnumrecibo.setText(String.valueOf(rec.getNumRecibo()));
            vista.txtnombre.setText(String.valueOf(rec.getNombre()));
            vista.txtfecha.setText(String.valueOf(rec.getFecha()));
            vista.txtkiloConsu.setText(String.valueOf(rec.getKiloConsu()));
            vista.txtcostoKilo.setText(String.valueOf(rec.getCostoKilo()));
            vista.jlImpuesto.setText(String.valueOf(rec.impuesto()));
            vista.jlTotalPagar.setText(String.valueOf(rec.totalPagar()));
            vista.jlsubTotal.setText(String.valueOf(rec.subTot()));
            
            }
            //cbx
            if (e.getSource() == vista.cbxtipoSer) {
                switch (vista.cbxtipoSer.getSelectedIndex()) {
                    case 0:
                        vista.txtcostoKilo.setText("2");
                        break;
                    case 1:
                        vista.txtcostoKilo.setText("3");
                        break;
                    case 2:
                        vista.txtcostoKilo.setText("5");
                        break;
                }
            }
        }
        /**
         * @param args the command line arguments
         */
    public static void main(String[] args) {
        // TODO code application logic here
        Recibo rec = new Recibo();
        JdlRecibo vista = new JdlRecibo(new JFrame(), true);
        Controlador cont = new Controlador(rec, vista);
        cont.iniciarVista();
    }

}
