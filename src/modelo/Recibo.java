/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo;

/**
 *
 * @author bmfil
 */
public class Recibo {
    
    private int numRecibo; 
    private String nombre;
    private String domicilio;
    private int  tipoServicio;
    private float costoKilo;
    private float kiloConsu;
    private String fecha;

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public Recibo(int numRecibo, String nombre, String domicilio, int tipoServicio, float costoKilo, float kiloConsu , String fecha) {
        this.numRecibo = numRecibo;
        this.nombre = nombre;
        this.domicilio = domicilio;
        this.tipoServicio = tipoServicio;
        this.costoKilo = costoKilo;
        this.kiloConsu = kiloConsu;
        this.fecha =  fecha;
    }

    public Recibo() {
        this.numRecibo = 0;
        this.domicilio = null;
        this.nombre = null; 
        this.tipoServicio= 0;
        this.costoKilo = 0.0f;
        this.kiloConsu = 0.0f;
        this.fecha = null;
        
    }
    public Recibo(Recibo rec){
        this.numRecibo = rec.numRecibo;
        this.domicilio = rec.domicilio;
        this.nombre =  rec.nombre;
        this.tipoServicio= rec.tipoServicio;
        this.costoKilo = rec.costoKilo;
        this.kiloConsu = rec.kiloConsu;
        this.fecha = rec.fecha;
    }

    public int getNumRecibo() {
        return numRecibo;
    }

    public void setNumRecibo(int numRecibo) {
        this.numRecibo = numRecibo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public int getTipoServicio() {
        return tipoServicio;
    }

    public void setTipoServicio(int tipoServicio) {
        this.tipoServicio = tipoServicio;
    }

    public float getCostoKilo() {
        return costoKilo;
    }

    public void setCostoKilo(float costoKilo) {
        this.costoKilo = costoKilo;
    }

    public float getKiloConsu() {
        return kiloConsu;
    }

    public void setKiloConsu(float kiloConsu) {
        this.kiloConsu = kiloConsu;
    }
    public float subTot(){
        return costoKilo * kiloConsu;
    }
    public float impuesto(){
        return this.subTot()*0.16f;
    }
    public float totalPagar(){
        return this.subTot() + this.impuesto();
    }
}
